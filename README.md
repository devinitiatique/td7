
# TD 7  -  Objets et classes  - Fraction et Date




##  Fraction



Le but de cet exercice est de concevoir une classe `Fraction`. Une
instance f = p/q de cette classe sera définie pour p et q des relatifs avec q != 0

Voici l’embryon de deux fichiers, l’un décrivant la classe Fraction, l’autre une classe cliente.

1. Posez toutes les questions jusqu’à comprendre toutes les lignes de ces classes.


```
public class Fraction {
    private int num;
    private int denom;

    public Fraction (int num, int denom) {
        this.num = num;
        this.denom = denom;
    }
    public String toString() {
    return this.num + "/" + this.denom;
    }   
}
public class MainFraction {
    public static void main(String args[]){
        Fraction f1, f2;
        f1 = new Fraction(24,36);
        f2 = new Fraction(13,26);
        System.out.println ("f1=" + f1.toString());
        System.out.println ("f2="  + f2.toString());
    }
}
```





2.  Ajouter à la classe `Fraction` une méthode `reduire` permettant de
    mettre une instance de `Fraction` sous forme réduite. Tester dans
    `MainFraction`.

    Indication : cette méthode modifie l'objet `this`.

    On pourra utiliser la méthode `pgcd` de la classe `Ut` (récupérable
    sur l'ENT) qui implémente l'algorithme d'Euclide.

3.  Ecrire comme variante une méthode :
    `public Fraction fractionReduite ()`

    qui retourne une nouvelle fraction correspondant à la fraction
    réduite de `this` (sans modifier `this`).

4.  Etant donnée une seconde fraction `f`, ajouter à la classe
    `Fraction` des méthodes (invoquées par les instances de `Fraction`)
    qui retournent :

    -   `this *  f`

    -   `this  +  f`

5.  En utilisant la méthode de produit de fractions ci-dessus, écrire
    une méthode `puissance` qui, étant donné un entier naturel $n$,
    calcule/retourne `this^n`.

Indication : pour les sous-questions 3, 4 et 5, on codera des méthodes
qui ne modifient pas la fraction `this` (ni `f`), mais qui renvoient une
nouvelle fraction.


      

##  Date

Concevoir une classe `Date` telle que, pour une instance `d_1` de cette
classe, on puisse :

1.  incrémenter la date `this` d'un jour

2.  connaître la date du lendemain (définir une fonction qui renvoie une
    nouvelle date)

3.  afficher la date `this` sous la forme : 14 mars 2020.

4.  Etant donnée une seconde date `d_2`, écrire des méthodes permettant
    de déterminer :

    -   si `this` est égale à `d_2`

    -   si `this` est antérieure à `d_2`

    -   si `this` est postérieure à `d_2`

    -   le nombre de jours séparant `this` de `d_2`

## Indications 

-   Vous pourrez définir une fonction `nbJoursMois` sans paramètre qui
    retourne le nombre de jours du mois de `this` (31 pour janvier, 30
    pour juin, 28 ou 29 pour février selon que l'année de `this` est
    bissextile ou non).

-   Vous pourrez également utiliser la méthode :\
    `public static boolean Ut.estBissextile(int an)` ou définir une
    méthode :

        /** Pré-requis: aucun
         *  Résultat:   retourne vrai ssi l'année de this est bissextile 
         */
        private boolean anneeEstBissextile()

-   Vous pourrez utiliser le tableau suivant dans certaines de vos
    méthodes.

        String[] moisLettres = {"janvier","fevrier","mars","avril", ...};

## Remarque pédagogique 

Pour éviter de déclarer `moisLettres` comme une variable locale dans
chacune des méthodes qui l'utilise, il est préférable de la déclarer une
seule fois, en haut de la classe `Date`, comme attribut de la classe,
aussi appelé *variable de classe*. En pratique, il faut ajouter le
mot-clef ` static` :

`private static String[] moisLettres = {"janvier", ...};`

Ainsi, ce tableau de classe est alloué une seule fois dans la mémoire de
la classe et n'est pas construit (copié) dans chaque instance de `Date`.
Une variable de classe est connue de et partagée par toutes les
instances (objets) de la classe.

## Dates en Java pour les curieux 

Plusieurs classes sont disponibles en Java pour gérer les dates. Nous
vous conseillons d'utiliser la classe `LocalDate` du paquetage
`java.time`, dont la description se trouve à l'URL :

`https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/LocalDate.html`

N'hésitez-pas à parcourir cette documentation par curiosité et y trouver
des idées d'extension de votre classe `Date`.
